#!/bin/bash

# Put NECOFS_GOM3_FORECAST.nc netcdf file in /home/ssuman/Desktop/
# Put mission1_uuids.json and  mission1_origin.yaml in /home/ssuman/Desktop/missions/
# Put start_time.txt in /home/ssuman/Desktop/conditions/ (the start time needs to be consistent with the NECOFS domain used

# Use the following line to start mission 1, with a start location near Woods Hole
#rocker --volume /home/ssuman/Desktop/:/home/robomaker/datasources//sim_resources/necofs_fvcom/ --volume /home/ssuman/Desktop/missions/:/home/robomaker/missions//Mission1/ --volume /home/ssuman/Desktop/missions/:/home/robomaker/missions//Mission2/ --volume /home/ssuman/Desktop/conditions/:/home/robomaker/conditions//Conditions/ --x11 --nvidia --user-override-name robomaker nopp7 roslaunch wind_config sim.launch start_x:=485515.93 start_y:=58133.25 start_z:=-1 mission_id:=1

# This is with the start location at point A of Discovery test - change mission_id to 1 or 2 to pick the mission to run. Node the volume missions is now mounted twice! One for Mission1 and one for Mission2 to match S3 config
rocker --volume /home/ssuman/Desktop/:/home/robomaker/datasources//sim_resources/necofs_fvcom/ --volume /home/ssuman/Desktop/missions/:/home/robomaker/missions//Mission1/ --volume /home/ssuman/Desktop/missions/:/home/robomaker/missions//Mission2/ --volume /home/ssuman/Desktop/conditions/:/home/robomaker/conditions//Conditions/ --x11 --nvidia --user-override-name robomaker nopp7 roslaunch wind_config sim.launch start_x:=479232.63 start_y:=56359.04 start_z:=-1 mission_id:=1


# Use the following line instead if you want to just drop into a shell promp after the entrypoint after starting the container
#rocker --volume /home/ssuman/Desktop/:/home/robomaker/datasources//sim_resources/necofs_fvcom/ --volume /home/ssuman/Desktop/missions/:/home/robomaker/missions//Mission1/ --volume /home/ssuman/Desktop/missions/:/home/robomaker/missions//Mission2/ --volume /home/ssuman/Desktop/conditions/:/home/robomaker/conditions//Conditions/ --x11 --nvidia --user-override-name robomaker nopp7 bash

# auv.kml logged into /var/log/ in the container so need to mount that as an additional docker volume if you want to access it on the host
# rosbags logged into /var/log/ in the container so need to mount that as an additional docker volume if you want to access it on the host
