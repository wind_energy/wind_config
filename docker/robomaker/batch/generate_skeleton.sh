#!/bin/bash

# SS - this prints to screen a skeleton of the json file structure to use for feeding the batch CLI

aws robomaker start-simulation-job-batch --generate-cli-skeleton
