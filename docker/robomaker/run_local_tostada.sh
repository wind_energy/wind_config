#!/bin/bash

#rocker --volume /home/ssuman/Desktop/:/opt/robomaker/datasources/fvcom/sim_resources/necofs_fvcom/ --x11 --nvidia --user-override-name robomaker nopp7 roslaunch wind_config sim.launch
#rocker --volume /mnt/data/Downloads/:/home/robomaker/datasources//sim_resources/necofs_fvcom/ --x11 --nvidia --user-override-name robomaker nopp7 roslaunch wind_config sim.launch

#rocker --volume /mnt/data/Downloads/:/home/robomaker/datasources//sim_resources/necofs_fvcom/ --volume /mnt/data/Downloads/missions/:/home/robomaker/missions//Mission1/  --volume /mnt/data/Downloads/missions/:/home/robomaker/missions//Mission2/ --volume /mnt/data/Downloads/conditions/:/home/robomaker/conditions//Conditions/ --volume /tmp/:/var/log/  --x11 --nvidia --user-override-name robomaker nopp7 bash

# revise locations to use something less volatile and preserve
S3RESOURCES="/home/jakuba/Dropbox/USBLOTIC/fieldwork/owttiusbl-2023-packard/s3_resources_local"
rocker --volume $S3RESOURCES/necofs_fvcom/:/home/robomaker/datasources//sim_resources/necofs_fvcom/ --volume $S3RESOURCES/Mission1/:/home/robomaker/missions//Mission1/  --volume $S3RESOURCES/Mission2/:/home/robomaker/missions//Mission2/ --volume $S3RESOURCES/Mission3/:/home/robomaker/missions//Mission3/   --volume $S3RESOURCES/Conditions/:/home/robomaker/conditions//Conditions/ --volume /tmp/:/var/log/  --x11 --nvidia --user-override-name robomaker nopp7 bash

# note - /tmp has too little space on tostada to run a complete sim.

# once logged in, to run weepeckets mission (probably in tmux window):
#roslaunch wind_config sim.launch start_x:=479232.63 start_y:=56359.04 start_z:=-1 mission_id:=1

# to run shelf mission 1, assume s3 bucket mounted at 
#roslaunch wind_config sim.launch start_x:=473904 start_y:=-4157 start_z:=-1 mission_id:=1
# gmrt bathy runs out n of this point.  start_y=15000 works.
# 2023/11/01 01:58:56 new GMRT bathy should cover everything.   but priorities not working - still sees only first entry.
# not seeing any batyh actually.

# updated mission1 and mission3 for buzzards bay survey with new waypts.
# roslaunch wind_config sim.launch start_x:=464399 start_y:=29529  start_z:=-1 mission_id:=1
# roslaunch wind_config sim.launch start_x:=464399 start_y:=29529  start_z:=-1 mission_id:=3

# updated launch for GPS1:
# roslaunch wind_config sim.launch start_x:=473904 start_y:=-4157 start_z:=-1 mission_id:=1
# roslaunch wind_config sim.launch start_x:=473904 start_y:=-4157 start_z:=-1 mission_id:=2
# roslaunch wind_config sim.launch start_x:=473904 start_y:=-4157 start_z:=-1 mission_id:=3

# updated launch for BB0 (missions 1 and 3 only):
# roslaunch wind_config sim.launch start_x:=458336 start_y:=41211 start_z:=-1 mission_id:=1
# roslaunch wind_config sim.launch start_x:=458336 start_y:=41211 start_z:=-1 mission_id:=3

# bf mapping
# envelope: minimum depth
# depth_floor: maximum depth
# altitude: minimum altitude
#
# shelf mission: 5 m min depth; 15 m altitude; 50 m max depth. (for transit).
