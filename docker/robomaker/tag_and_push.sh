#!/bin/bash

#docker tag nopp7:latest 517795548087.dkr.ecr.us-east-1.amazonaws.com/nopp7:latest
#docker push 517795548087.dkr.ecr.us-east-1.amazonaws.com/nopp7:latest


docker tag nopp7:latest public.ecr.aws/h7x3h2i0/nopp7:latest
docker push public.ecr.aws/h7x3h2i0/nopp7:latest


# Remove previous untagged images now that we have a new one
docker image prune --filter="dangling=true"
