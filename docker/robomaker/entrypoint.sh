#!/bin/bash
set -e

#if [ ! -z $GAZEBO_MASTER_URI ]; then
#        tmp_GAZEBO_MASTER_URI=$GAZEBO_MASTER_URI
#fi

source /opt/ros/melodic/setup.bash

source /usr/share/gazebo-9/setup.sh

cd /ws/devel
source ./setup.bash

#if [ ! -z $tmp_GAZEBO_MASTER_URI ]; then
#        export GAZEBO_MASTER_URI=$tmp_GAZEBO_MASTER_URI
#        unset tmp_GAZEBO_MASTER_URI
#fi

export GAZEBO_MODEL_PATH=/ws/src/wind_config/world/:/data/gmrt_nes_bathy_tiles/:/data/ncei_bathy_tiles/:${GAZEBO_MODEL_PATH}
export GAZEBO_RESOURCE_PATH=/ws/src/wind_config/world/:/data/gmrt_nes_bathy_tiles/:/data/ncei_bathy_tiles/:${GAZEBO_RESOURCE_PATH}

printenv

echo "Contents of mounted data source bucket: "
ls /home/robomaker/datasources
#ls /home/robomaker/datasources/fvcom
#ls /home/robomaker/datasources/fvcom/sim_resources
#ls /home/robomaker/datasources/fvcom/sim_resources/necofs_fvcom
ls /home/robomaker/datasources/
ls /home/robomaker/datasources/sim_resources
ls /home/robomaker/datasources/sim_resources/necofs_fvcom

echo "Bathy ls: "
ls /data/ncei_bathy_tiles/

exec "${@:1}"

