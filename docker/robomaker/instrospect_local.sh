#!/bin/bash

# The first command line argument needs to be the container name from "docker ps"

docker exec -it $1 bash
