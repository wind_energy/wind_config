# README for Robomaker

## Relevant links to bookmark:
* Robomaker: https://us-east-1.console.aws.amazon.com/robomaker/home?region=us-east-1#simulationJobs contains all the simulations setup and leverages other AWS services like ECR and S3
* ECR (Elastic Container Registry, essentially the AWS-equivalent of Docker Hub that stores docker images): https://us-east-1.console.aws.amazon.com/ecr/repositories?region=us-east-1 contains the nopp7:latest docker image pushed
* S3 storage: https://s3.console.aws.amazon.com/s3/home?region=us-east-1 contains the "nes-sim-data" bucket that contains the sim resources (bathy, current model, etc..) and "nopp7" bucket that is the log/storage output for the robomaker simulations
* IAM (Identity and Access Management): https://us-east-1.console.aws.amazon.com/iamv2/home?region=us-east-1#/home establishes policies on what resources certin users and groups can access
* AWS CLI (command line interface): https://aws.amazon.com/cli/ this needs to be installed on the host computer to be able to interact with AWS resources remotely via shell/scripts (not UI) and to authenticate/logout of AWS resources

## Relevant files in this folder:
* authenticate_aws.sh (run to authenticate the host PC with the AWS services to allow push/pulls from the ECR registry).  Run 'aws configure' to set up credentials the first time.  A new AWS Access Key must be generated to view the Secret Access Key also required.
* logout_aws.sh (logout local docker daemon from the remote ECR)
* Dockerfile and entrypoint.sh (these two contain the build steps for the simulation application docker image)
* build.sh (build the docker image. NOTE: needs to be authenticated with aws by running authenticate_aws.sh otherwise it can't pull the ros melodic base image from ECR)
* tag_and_push.sh (tags the built image with nopp7:latest and pushes it to AWS ECR)

## Typical build flow:
*typical commands sequence after changes are done to either code or Dockerfile: authenticate_aws.sh -> build.sh -> tag_and_push.sh
*NOTES: sometimes AWS CLi gets stuck in "not authenticated"(if that's the case the build will fail because it can't pull the base ros melodic image from ECR), if that happens runs logout_aws.sh and then authenticate_aws.sh until it's authenticated

## Trying to run a simulation:
* Go to the simulation jobs page on robomaker (https://us-east-1.console.aws.amazon.com/robomaker/home?region=us-east-1#simulationJobs), click on one of the jobs that have status canceled or completed, then on the top right under Actions select "Clone": this will allow to pre-populate a new simulation job with good configuration, that can then be edited as needed.

## TIPS AND TRICKS:
* Remember that launched simulations jobs will have to be stopped otherwise will run up to the configured max simulation duration (8 hours in the config that I did) which will use $/CPU hours
* This page https://us-east-1.console.aws.amazon.com/robomaker/home?region=us-east-1#simulationApplications defines the nopp7-sim simulation application that is used when creating the simulation job. Essentially it instructs Robomaker to get the nopp7:latest docker image from the public ECR as the simulation application