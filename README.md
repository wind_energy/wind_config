# lrauv_config

The system uses wstool to initialize the ros workspace and pull changes from the repositories.

To bootstrap the install system, you need the rosinstall file in the wstool folder of the wstool folder in this repository.

1. Create a workspace folder in your local filesystem, i.e. ~/ros/lrauv_ws and a src folder within it, i.e. ~/ros/lrauv_ws/src
2. In ~/ros/lrauv_ws, type "wstool init src PATH_TO_ROSINSTALL_FILE.rosinstall" (or copy the rosinstall file n the src folder, renaming it to .rosinstall)
3. In ~/ros/lrauv_ws, type "wstool update -t src"
4. In ~/ros/lrauv_ws, type "catkin_make"

## Environment setup

Most of the larger world-related files are not stored in repositories, but in a dropbox folder in the 2019catalyst_wind project. It is so assumed that the user has a symbolink link in the root filesystem, called /dropbox, that points to the root folder of the local dropbox mirror, i.e.

```
ssuman@desk:/$ sudo ln -s /home/ssuman/Dropbox\ \(WHOI\ DSL\)/ /dropbox
[sudo] password for ssuman: 
ssuman@desk:/$ ls -la | grep dropbox
lrwxrwxrwx   1 root   root        32 Jan 22 12:11 dropbox -> /home/ssuman/Dropbox (WHOI DSL)/
ssuman@desk:/$ cd /dropbox
ssuman@desk:/dropbox$ ls -la
total 280
drwx------  4 ssuman ssuman   4096 Jan 21 12:09 .
drwxr-xr-x 63 ssuman ssuman   4096 Jan 17 11:07 ..
drwxrwxr-x  3 ssuman ssuman   4096 Jan 21 12:09 2019catalyst_wind
-rwxrwxr-x  1 ssuman ssuman     35 Jan 17 11:07 .dropbox
drwxrwxr-x  7 ssuman ssuman  12288 Jan 21 12:09 .dropbox.cache
-rw-rw-r--  1 ssuman ssuman 246000 Feb 16  2012 Getting Started.pdf
```
